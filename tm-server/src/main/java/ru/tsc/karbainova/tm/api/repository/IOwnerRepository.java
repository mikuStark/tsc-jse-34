package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;
import ru.tsc.karbainova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findById(String userId, String id);

    E removeById(String userId, String id);

    void add(String userId, E entity);

    void remove(String userId, E entity);

    void addAll(List<E> entities);

    void clear();
}
