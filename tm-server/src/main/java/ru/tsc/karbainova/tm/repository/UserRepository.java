package ru.tsc.karbainova.tm.repository;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.IUserRepository;
import ru.tsc.karbainova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NonNull
    public final Predicate<User> predicateByLogin(@NonNull final String login) {
        return s -> login.equals(s.getLogin());
    }

    @NonNull
    public final Predicate<User> predicateByEmail(@NonNull final String email) {
        return s -> email.equals(s.getEmail());
    }

    @NonNull
    public final Predicate<User> predicateById(@NonNull final String id) {
        return s -> id.equals(s.getId());
    }

    @Override
    public User add(final User user) {
        entities.add(user);
        return user;
    }

    @Override
    public User findById(@NonNull final String id) {
        return entities.stream()
                .filter(predicateById(id))
                .findFirst().orElse(null);
    }

    @Override
    public User findByLogin(@NonNull final String login) {
        return entities.stream()
                .filter(predicateByLogin(login))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(@NonNull final String email) {
        return entities.stream().filter(predicateByEmail(email)).findFirst().orElse(null);
    }

    @Override
    public User removeUser(@NonNull final User user) {
        entities.remove(user);
        return user;
    }

    @Override
    public User removeById(@NonNull final String id) {
        final User user = findById(id);
        return removeUser(user);
    }

    @Override
    public User removeByLogin(@NonNull final String login) {
        final User user = findByLogin(login);
        return removeUser(user);
    }
}
