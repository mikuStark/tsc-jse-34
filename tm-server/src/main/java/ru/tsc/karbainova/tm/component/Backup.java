package ru.tsc.karbainova.tm.component;

import lombok.NonNull;
import lombok.SneakyThrows;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ru.tsc.karbainova.tm.command.data.BackupLoadCommand.BACKUP_LOAD;
import static ru.tsc.karbainova.tm.command.data.BackupSaveCommand.BACKUP_SAVE;

public class Backup {

    @NonNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NonNull
    private final Bootstrap bootstrap;

    public Backup(@NonNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private static final int interval = 30;

    private void save() {
        bootstrap.executeCommand(BACKUP_SAVE);
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, interval, TimeUnit.SECONDS);
    }

    private void load() {
        bootstrap.executeCommand(BACKUP_LOAD);
    }

    public void stop() {
        es.shutdown();
    }

}
