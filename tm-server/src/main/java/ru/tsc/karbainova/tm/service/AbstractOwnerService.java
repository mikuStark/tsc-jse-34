package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.api.service.IOwnerService;
import ru.tsc.karbainova.tm.api.service.IService;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;
import ru.tsc.karbainova.tm.repository.AbstractRepository;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {
    protected IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super((IRepository) repository);
        this.repository = repository;
    }

    @Override
    public List<E> findAll(String userId) {
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(String userId, Comparator<E> comparator) {
        return repository.findAll(userId, comparator);
    }

    @Override
    public E findById(String userId, String id) {
        return repository.findById(userId, id);
    }

    @Override
    public E removeById(String userId, String id) {
        return repository.removeById(userId, id);
    }

}
