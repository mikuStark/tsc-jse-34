package ru.tsc.karbainova.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.ISignatureSetting;

@UtilityClass
public class HashUtil {
    @NonNull
    String SECRET = "1231231231";

    Integer ITERATION = 12345;

    @Nullable
    public static String sign(@NonNull final ISignatureSetting setting, @NonNull final Object value) {
        try {
            @NonNull final ObjectMapper objectMapper = new ObjectMapper();
            @NonNull final String json = objectMapper.writeValueAsString(value);
            return salt(json, setting.getSignatureIteration(), setting.getSignatureSecret());
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @NonNull
    public static String salt(
            @Nullable final ISaltSettings settings,
            @Nullable final String value) {
        if (settings == null) return null;
        @Nullable final String secret = settings.getPasswordSecret();
        @Nullable final Integer iteration = settings.getPasswordIteration();
        return salt(value, iteration, secret);
    }

    @Nullable
    static String salt(@Nullable final String s,
                       @NonNull Integer iteration,
                       @NonNull String secret) {
        if (s == null) return null;
        String result = s;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    public static String md5(@NonNull final String value) {
        try {
            @NonNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NonNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
