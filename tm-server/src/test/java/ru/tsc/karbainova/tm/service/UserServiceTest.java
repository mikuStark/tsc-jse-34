package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.repository.UserRepository;

import java.util.List;

public class UserServiceTest {
    private UserService userService;
    private User user;
    private final String userLogin = "test";

    @Before
    public void before() {
        userService = new UserService(new UserRepository(), new PropertyService());
        User user = new User();
        user.setLogin(userLogin);

        this.user = userService.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());

        @NonNull final User projectById = userService.findById(user.getId());
        Assert.assertNotNull(projectById);
    }


    @Test
    public void findAll() {
        @NonNull final List<User> users = userService.findAll();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findById() {
        Assert.assertNotNull(userService.findById(user.getId()));
    }

    @Test
    public void findByLogin() {
        Assert.assertNotNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void remove() {
        userService.remove(this.user);
        Assert.assertEquals(0, userService.findAll().size());
        Assert.assertNull(userService.findByLogin(user.getLogin()));
    }
}
