package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    @Nullable
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    @NonNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public AbstractCommand getCommandByName(@NonNull String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(@Nullable String arg) {
        return commands.get(arg);
    }

    @NonNull
    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Nullable
    @Override
    public Map<String, AbstractCommand> getArguments() {
        return arguments;
    }

    @Override
    public Collection<String> getCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public void create(AbstractCommand command) {
        final String name = command.name();
        final String arg = command.arg();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }
}
